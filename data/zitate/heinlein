Dieses ist eine verrückte Welt und der einzige Weg, sie zu genießen ist,
sie als Witz zu betrachten.
		-- Robert Anson Heinlein (The Number of the Beast)
%
Jeder Priester oder Schamane sollte als schuldig gelten, solange nicht das
Gegenteil bewiesen wird.
		-- Robert Anson Heinlein (Die Leben des Lazarus Long)
%
Höre immer auf den Rat der Experten! Sie sagen dir, was auf keinen Fall geht
und weshalb es nicht geht. Wenn du das weißt, mach dich an die Arbeit!
		-- Robert Anson Heinlein (Die Leben des Lazarus Long)
%
Läßt sich etwas nicht in Zahlen ausdrücken, so ist es keine Wissenschaft,
sondern eine Ansicht.
		-- Robert Anson Heinlein (Die Leben des Lazarus Long)
%
Eine Generation, welche der Geschichte keine Beachtung schenkt, hat keine
Vergangenheit - und keine Zukunft.
		-- Robert Anson Heinlein (Die Leben des Lazarus Long)
%
Auf keiner Welt und in keiner Epoche zeigt die Geschichte eine Religion
auf, die eine rationale Grundlage besitzt. Die Religion ist eine Krücke
für diejenigen, welche das Unbekannte nicht aus eigener Kraft meistern.
Dennoch haben die meisten Leute eine Religion, wie man Schuppen hat,
vergeuden Zeit und Geld dafür und finden es schön, damit herumzutun.
		-- Robert Anson Heinlein (Die Leben des Lazarus Long)
%
Cheopsgesetz: Kein Bauwerk wird innerhalb der gesetzten Frist und im
Rahmen des Kostenvoranschlags fertig gestellt.
		-- Robert Anson Heinlein (Die Leben des Lazarus Long)
%
Von all den merkwürdigen "Verbrechen", welche die Menschheit aus dem
Nichts konstruiert hat, ist "Gotteslästerung" das verrückteste, dicht
gefolgt von "obszönem Betragen" und "Exhibitionismus".
		-- Robert Anson Heinlein (Die Leben des Lazarus Long)
%
Den Menschen gelingt es selten (wenn überhaupt), einen Gott ins Leben
zu rufen, der ihnen überlegen ist. Die meisten Götter haben da Benehmen
und die Moral eines verzogenen Kindes.
		-- Robert Anson Heinlein (Die Leben des Lazarus Long)
%
Appelliere nie an die "bessere Natur" eines Menschen. Vielleicht hat
er keine. Sein Eigennutz bietet einen besseren Ansatzpunkt.
		-- Robert Anson Heinlein (Die Leben des Lazarus Long)
%
Im gesamten Verlauf der Geschichte ist Armut der Normalzustand der
Menschheit. Vorstöße, die dazu beitragen, diese Norm zu überwinden - hin
und wieder, hie und da-, sind das Werk einer außerordentlich kleinen
Minderheit, häufig verachtet, oft verflucht und fast immer von allen
rechtdenkenden Menschen angefeindet. Wo immer dieser Minderheit Einhalt
geboten wird oder wo sie (wie es gelegentlich geschieht) aus der
Gemeinschaft verstoßen wird, sinkt das Volk in bittere Armut.  Das nennt
man dann "Pech".
		-- Robert Anson Heinlein (Die Leben des Lazarus Long)
%
In einer hoch entwickelten Zivilisation ist "Diener des Volkes"
gleichbedeutend mit "Herr des Volkes".
		-- Robert Anson Heinlein (Die Leben des Lazarus Long)
%
Wenn die Bevölkerungsdichte auf einer Welt so zunimmt, daß man Ausweise
benötigt, ist der soziale Zusammenbruch nicht mehr fern. Es wird höchste
Zeit, auszuwandern. Das Beste an der Raumfahrt ist, daß sie
Auswanderungsmöglichkeiten geschaffen hat.
		-- Robert Anson Heinlein (Die Leben des Lazarus Long)
%
Das Zweitbeste an der Raumfahrt ist, daß sich bei den großen
Entfernungen Kriege als schwierig, unpraktisch und fast immer als
unnötig erweisen. Das mag für viele ein Verlust sein, denn der Krieg ist
der beliebteste Volkssport unserer Rasse, er gibt dem langweiligen,
stumpfen Alltag Zweck und Farbe. Aber es ist eine herrliche Entwicklung
für den intelligenten Menschen, der nur dann kämpft, wenn er keine
andere Wahl hat - niemals jedoch zum Spaß.
		-- Robert Anson Heinlein (Die Leben des Lazarus Long)
%
Demokratie basiert auf der Annahme, daß eine Million Menschen klüger
sind als ein Mensch. Wie war das gleich? So ganz begreife ich die Logik
nicht.
Autokratie basiert auf der Annahme, daß ein Mensch klüger ist als eine
Million Menschen. Sehen wir uns das noch einmal an. Wer entscheidet?
		-- Robert Anson Heinlein (Die Leben des Lazarus Long)
%
Jede Regierung funktioniert, wenn Machtbefugnis und Verantwortung gleich
groß und aufeinander abgestimmt sind. Das garantiert nicht unbedingt
eine "gute" Regierung; es garantiert nur, daß sie funktioniert. Aber
solche Regierungen sind selten - die meisten Leute wollen zwar mitreden,
scheuen jedoch den Vorwurf, wenn etwas schief geht. Früher nannte man
diese Haltung "Beifahrersyndrom".
		-- Robert Anson Heinlein (Die Leben des Lazarus Long)
%
Wie steht es mit den Fakten? Wieder und immer wieder - wie steht es mit
den Fakten? Vermeide jedes Wunschdenken, achte nicht auf die göttliche
Erleuchtung, vergiß, was "die Sterne sagen", lass dich nicht von
Meinungen beeinflussen, nimm keine Rücksicht auf die Nachbarn - stelle
dir nur eine Frage: Wie steht es mit den Fakten? Du steuerst immer in
eine unbekannte Zukunft. Fakten sind dein einziger Anhaltspunkt. Trage
sie zusammen.
		-- Robert Anson Heinlein (Die Leben des Lazarus Long)
%
Dummheit kann man weder durch Geld noch durch Erziehung oder Gesetze
abhelfen. Dummheit ist keine Sünde; das Opfer kann nichts dafür, daß es
dumm ist. Aber Dummheit stellt das einzige Kapitalverbrechen des
Universums dar. Das Urteil lautet auf Tod, es gibt keine Berufung, und
die Exekution erfolgt automatisch und ohne Erbarmen.
		-- Robert Anson Heinlein (Die Leben des Lazarus Long)
%
Mut ist die Ergänzung zu Furcht. Jemand, der furchtlos ist, kann nicht
mutig sein. (Nebenbei ist er ein Trottel.)
		-- Robert Anson Heinlein (Die Leben des Lazarus Long)
%
Die Wahrheit einer Behauptung hat nichts mit ihrer Glaubwürdigkeit zu
tun. Und umgekehrt.
		-- Robert Anson Heinlein (Die Leben des Lazarus Long)
%
Bewegliche Teile, die aneinander reiben, müssen geschmiert werden, damit
sie sich nicht abnützen. Wo Menschen aneinander reiben, bedient man sich
der Ehrentitel und höflichen Floskeln als Schmiermittel. Jugendliche,
naive und schlichte Gemüter, aber auch Leute, die nicht viel
herumgekommen sind, betrachten diese Formalitäten als "leer", "sinnlos"
und "entwürdigend" und weigern sich, sie zu benutzen. So "rein" ihre
Beweggründe auch sein mögen, sie werfen durch ihr Verhalten Sand in ein
Getriebe, das ohnehin nur mühsam in Bewegung zu halten ist.
		-- Robert Anson Heinlein (Die Leben des Lazarus Long)
%
Hüte dich vor Altruismus! Er basiert auf Selbstbetrug, der Wurzel allen
Übels.
Bist du versucht etwas zu tun, das nach "Altruismus" riecht, so
durchleuchte deine Beweggründe und merze den Selbstbetrug aus. Bleibt
dein Entschluß auch danach fest - bitte, nur hinein ins Vergnügen.
		-- Robert Anson Heinlein (Die Leben des Lazarus Long)
%
Die verrückteste Idee, die der Homo sapiens je hervorgebracht hat, ist
die: daß unser Herrgott, der Weltenregierer und Schöpfer des Alls, auf
die kitschige Anbetung seiner Untertanen angewiesen sei, daß er sich
von ihren Gebeten beeinflussen ließe und schmollte, wenn sie ihm nicht
schmeicheln. Und doch finanziert diese absurde Wahnvorstellung, für die
es nicht die Spur eines Beweises gibt, sie älteste, größte und
unproduktivste Industrie der Menschheitsgeschichte.
		-- Robert Anson Heinlein (Die Leben des Lazarus Long)
%
Frauen sind unweigerlich die Verlierer, wenn sie auf absoluter
"Gleichheit" mit dem Manne bestehen. Was sie sind und was sie vermögen,
macht sie dem Mann überlegen, und die richtige Taktik wäre es, auf Grund
dieser Tatsache so viele Sonderrechte wie möglich zu erpressen. Mit
"Gleichheit" sollten sie sich nicht zufrieden geben. "Gleichheit"
bedeutet für Frauen eine Katastrophe.
		-- Robert Anson Heinlein (Die Leben des Lazarus Long)
%
Benachteilige deine Kinder nicht, indem du ihnen das Leben zu leicht
machst.
		-- Robert Anson Heinlein (Die Leben des Lazarus Long)
%
Man sollte nie die ungeheure Macht der menschlichen Dummheit unterschätzen.
		-- Robert Anson Heinlein (Die Leben des Lazarus Long)
%
Falls sich im Laufe einer Familienstreits herausstellt, das du im Recht
bist - entschuldige dich sofort!
		-- Robert Anson Heinlein (Die Leben des Lazarus Long)
%
Wer jung bleiben will, muss es immer wieder fertig bringen, mit den
Fehlern von früher zu brechen.
		-- Robert Anson Heinlein (Die Leben des Lazarus Long)
%
Zeigt die Geschichte einen einzigen Fall auf, in dem die Mehrheit recht
hatte?
		-- Robert Anson Heinlein (Die Leben des Lazarus Long)
%
Nur ein Sadist - oder ein Idiot - sagt bei gesellschaftlichen Anlässen
die nackte Wahrheit.
		-- Robert Anson Heinlein (Die Leben des Lazarus Long)
%
Der Unterschied zwischen Wissenschaft und verschwommenen Theorien
besteht darin, dass man die Wissenschaft nur mit Logik bewältigt,
während für die anderen Dinge Gelehrtentum ausreicht.
		-- Robert Anson Heinlein (Die Leben des Lazarus Long)
%
Der Elementarsinn ist das Tasten. Ein Baby tastet, noch bevor es geboren
wird und lange bevor es die Seh-, Gehör-, und Geschmacksnerven
einzusetzen lernt. Kein Mensch kommt ohne Tastsinn aus. Gib deinen
Kindern wenig Taschengeld - aber umarme sie um so öfter.
		-- Robert Anson Heinlein (Die Leben des Lazarus Long)
%
Geheimhaltung ist der erste Schritt zur Tyrannei.
		-- Robert Anson Heinlein (Die Leben des Lazarus Long)
%
Die produktivste Kraft ist der menschliche Egoismus.
		-- Robert Anson Heinlein (Die Leben des Lazarus Long)
%
Vereinfache deine Arbeits- und Denkprozesse! Das verlängert deins
effektives Leben und gibt dir Zeit, dich über Schmetterlinge, junge
Kätzchen und Regenbogen zu freuen.
		-- Robert Anson Heinlein (Die Leben des Lazarus Long)
%
Sachkenntnis auf einem Gebiet überträgt sich nicht automatisch auf
andere Gebiete. Leider glauben das viele Experten - und je enger
begrenzt ihr Wissen ist, desto fester glauben sie daran.
		-- Robert Anson Heinlein (Die Leben des Lazarus Long)
%
Wehre dich nicht allzu standhaft gegen eine Versuchung; wer weiß,
wann die nächste kommt ...
		-- Robert Anson Heinlein (Die Leben des Lazarus Long)
%
Das unbegründete Wecken eines Schläfers ist erst im Wiederholungsfalle
als Kapitalverbrechen zu werten.
		-- Robert Anson Heinlein (Die Leben des Lazarus Long)
%
Eine Frau, die einen Mann wieder aufrichten kann, muss nicht schön sein.
Doch über kurz oder lang wird dieser Mann feststellen, dass sie schön
ist - es war ihm nur anfangs entgangen.
		-- Robert Anson Heinlein (Die Leben des Lazarus Long)
%
Ein Stinktier ist eine bessere Gesellschaft, als ein Mensch, der sich
damit brüstet, "offen und ehrlich" zu sein.
		-- Robert Anson Heinlein (Die Leben des Lazarus Long)
%
Naturgesetze kennen kein Mitleid.
		-- Robert Anson Heinlein (Die Leben des Lazarus Long)
%
Wenn es etwas ist, das "jeder weiß", dann stimmt es garantiert nicht.
		-- Robert Anson Heinlein (Die Leben des Lazarus Long)
%
Dinge, die man umsonst bekommt, werden oft über ihren Wert bezahlt.
		-- Robert Anson Heinlein (Die Leben des Lazarus Long)
%
Pessimist aus taktischen Gründen, Optimist vom Charakter her - man kann
beides sein. Wie? Indem man kein unnötiges Risiko eingeht und die
Gefahren, die sich vermeiden lassen, auf ein Minimum reduziert. So kann
man das Spiel gelassen zu Ende führen, ohne sich um den Ausgang zu
sorgen.
		-- Robert Anson Heinlein (Die Leben des Lazarus Long)
%
Verwechsle "Pflicht" nicht mit dem, was die anderen von dir erwarten;
das sind zwei völlig verschiedene Dinge. Pflicht ist die Erfüllung von
Aufgaben, die du dir selbst gestellt hast. Das kann eine menge von dir
fordern - von jahrelanger geduldiger Arbeit bis zu der Bereitschaft, das
Leben zu opfern. Nicht einfach, gewiss, aber der Lohn ist Selbstachtung.
  Auf der anderen Seite bringt es überhaupt keinen Lohn, das zu tun, was
die anderen von dir erwarten; es ist nicht nur schwer - es ist geradezu
unmöglich. Leichter schüttelst du noch einen Straßenräuber ab als den
Schmarotzer, der deine zeit "nur ein paar Minuten in Anspruch nehmen"
will. Die Zeit stellt dein ganzes Kapital dar, und die Minuten deines
Lebens sind gezählt. Machst du es dir zur Gewohnheit, solchen Bitten
Gehör zu schenken, dann stürzen sich diese Kerle wie Blutegel auf dich.
  Lerne also Nein zu sagen - und bleibe hart!
  Andernfalls hast du nicht genug Zeit, um deine Pflicht zu erfüllen
oder deine eigene Arbeit zu tun; ganz bestimmt hast du nicht genug Zeit
für Liebe und Glück. Die Termiten fressen an deinem Leben und höhlen es
aus.
  (Das heißt nun nicht, dass du einem Freund oder auch einem Fremden
keinen Gefallen erweisen darfst. Aber triff du die Wahl! Tu es nicht,
weil man es von dir "erwartet"!)
		-- Robert Anson Heinlein (Die Leben des Lazarus Long)
%
Du kannst 1000 Menschen schneller schwankend machen, indem du an ihre
Vorurteile appellierst, als Du einen einzigen Menschen durch Logik
überzeugst.
		-- Robert Anson Heinlein (Revolte im Jahr 2100)
%
