Alle guten Dinge sind billig; alle schlechten sind teuer.
		-- Henry David Thoreau
%
Die Behauptung, jemand sei ein Freund, besagt in der Regel nicht mehr,
als daß er kein Feind ist.
		-- Henry David Thoreau
%
Es genügt nicht, nur fleißig zu sein - das sind die Ameisen. Die
Frage ist vielmehr: wofür sind wir fleißig?
		-- Henry David Thoreau
%
Jede Generation lacht über Moden, aber folgt den neuen treu.
		-- Henry David Thoreau
%
Hast Du Luftschlösser gebaut,
so braucht Deine Arbeit
nicht verloren zu sein.
Eben dort sollten sie sein.
Jetzt lege das Fundament darunter.
		-- Henry David Thoreau
%
Es werden zwei gebraucht, um die Wahrheit zu sprechen - einen der
spricht und einen anderen der hört.
		-- Henry David Thoreau
%
"Das Gesetz hat die Menschen nicht um ein Jota gerechter gemacht;
gerade durch ihren Respekt vor ihm werden auch die Wohlgesinnten
jeden Tag zu Handlangern des Unrechts."
		-- Henry David Thoreau
%
In einem Staat, der seine Bürger willkürlich einsperrt, ist es eine
Ehre für einen Mann, im Gefängnis zu sitzen."
		-- Henry David Thoreau
%
Könnte es nicht eine Regierung geben, in der nicht die Mehrheit über
Falsch und Richtig befindet, sondern das Gewissen? [...] Muss der Bürger
auch nur einen Augenblick, auch nur ein wenig, sein Gewissen dem Gesetzgeber
überlassen? Wozu hat denn jeder Mensch ein Gewissen? Ich finde wir sollten
erst Menschen sein, und danach Untertanen.
		-- Henry David Thoreau
%
Wenn ich einem Ertrinkenden das Holzbrett entrissen habe, mit dem er sich
über Wasser gehalten hat, dann muss ich es ihm zurückgeben, und wenn ich
dabei selbst ertrinke.
		-- Henry David Thoreau
%
Noch immer leben wir niedrig wie Ameisen, obgleich die Sage erzählt,
wir seien schon vor langer Zeit in Menschen verwandelt worden.
		-- Henry David Thoreau
%
Man sollte nicht den Respekt vor dem Gesetz pflegen, sondern vor der Gerechtigkeit.
		-- Henry David Thoreau
%
Überflüssiger Reichtum kann nur Überflüssiges erkaufen.
		-- Henry David Thoreau
%
Wenn wir uns von unseren Träumen leiten lassen, wird der Erfolg all unsere
Erwartungen übertreffen.
		-- Henry David Thoreau
%
Die Wege, auf denen man Geld gewinnen kann, führen fast ausnahmslos abwärts.
Wenn du etwas getan hast, wodurch du bloß Geld verdient hast, so bist du
wahrlich faul oder noch schlimmer gewesen.
		-- Henry David Thoreau
%
Unsere Feinde sind in unserer Mitte und überall um uns... denn unser Feind
ist die fast universelle Starrheit von Kopf und Herz, der Mangel an Vitalität
im Menschen.
		-- Henry David Thoreau
%
Als ob man die Zeit totschlagen könnte, ohne die Ewigkeit zu verletzen.
		-- Henry David Thoreau
%
